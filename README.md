# 227-0653-00 - Quantum Measurements and Optomechanics - Exercise solutions

The solutions are organized by problem sheet and are written in python and jupyter notebooks.


## Installation instructions

 The required libraries (numpy, scipy, matplotlib, ipywidgets, qutip) can be installed using pip or conda:
```
pip install -r requirements.txt
```
or
```
conda install --file requirements.txt
```
