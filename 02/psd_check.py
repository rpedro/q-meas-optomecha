from pathlib import Path

from numpy import absolute, arange, float64, loadtxt, mean, pi, var, zeros
from numpy.fft import fft, fftfreq
from numpy.typing import NDArray
from scipy.optimize import least_squares, OptimizeResult
from scipy.signal import welch
from matplotlib.pyplot import close, subplots

SAMPLING_RATE = 1e6
DT = 1e-6
SFOLDER = Path(__file__).parent


def plot_signals(
    signals: NDArray[float64], 
    cols: list[int]) -> None:
    
    N = signals.shape[0]
    T = arange(0.0, DT * N, DT) * 1e3
    
    fig, axs = subplots()
    axs.set_title("Selected time traces")
    axs.set_xlabel("Time [ms]")
    axs.set_ylabel("Current [A]")
    axs.grid(True)
    
    for col in cols:
        axs.plot(T, signals[:, col], label=f"{col}")

    axs.legend(loc="upper right")
    fig.tight_layout()
    fig.savefig(SFOLDER / "Signal_Plots.png", dpi=450)
    close(fig)


def plot_psd_w_fit(
    freqs: NDArray[float64], 
    Sxx: NDArray[float64],
    freqs_welch: NDArray[float64],
    Sxx_welch: NDArray[float64], 
    RB: NDArray[float64],
    RB_welch: NDArray[float64]
) -> None:

    fig, axs = subplots(ncols=1, nrows=2, sharex=True, sharey=True, figsize=(6, 6))

    for i, (f, S, rb, method) in enumerate(
        [
            (freqs, Sxx, RB, "Avg. FFT^2"), 
            (freqs_welch, Sxx_welch, RB_welch, "Welch")
        ]
    ):

        R, B = rb
        axs[i].set_title(f"Switching rate: {1e-3 * R:.1f} kHz, "
                         f"White noise: {1e6 * B:.1f}"
                         fr" (mA)$^2$ / Hz")
        axs[i].set_xlabel("Frequency [kHz]")
        axs[i].set_ylabel(r"PSD Current [(mA)$^2$ / Hz]")
        axs[i].grid(True)

        FIT_SOL = eval_lorentz(freqs=f, r=R) + B
        axs[i].plot(1e-3 * f, 1e6 * S, label=method)
        axs[i].plot(1e-3 * f, 1e6 * FIT_SOL, label="Curve fit")
        axs[i].legend(loc="upper right")

    fig.tight_layout()
    fig.savefig(SFOLDER / "Averaged_PSD.png", dpi=450)
    close(fig)


def psd_welch(signals: NDArray[float64]) -> tuple[NDArray[float64], NDArray[float64]]:

    freq, Sxx = welch(signals.flatten(order="F"), fs=SAMPLING_RATE, nperseg=10000)
    
    return freq[freq > 0], Sxx[freq > 0]


def psd_fft(signal: NDArray) -> NDArray[float64]:
    # https://en.wikipedia.org/wiki/Spectral_density#Power_spectral_density
    T_TOTAL = DT * signal.shape[0]
    Sxx = absolute(fft(signal)) ** 2 * DT ** 2 / T_TOTAL
    
    # WE NEED THE SINGLE-SIDED SPECTRAL DENSITY, HENCE THE FACTOR OF TWO
    Sxx *= 2

    return Sxx


def averaged_psd(signals: NDArray) -> NDArray[float64]:

    (N_pts, N_cols) = signals.shape
    FREQS = fftfreq(n=N_pts, d=DT)
    avg_Sxx = zeros(shape=(N_pts), dtype=float64)

    for col in range(N_cols):
        Sxx = psd_fft(signals[:, col])    
        avg_Sxx += Sxx

    avg_Sxx /= N_cols
    
    return FREQS[FREQS > 0], avg_Sxx[FREQS > 0]


def signal_variances(signals: NDArray) -> NDArray[float64]:

    return mean(var(signals, axis=0, ddof=1))


def psd_variance(freqs: NDArray[float64], Sxx: NDArray[float64]) -> float:

    DF = freqs[1] - freqs[0]
    return sum(Sxx) * DF


def eval_lorentz(freqs: NDArray, r: float) -> NDArray[float64]:

    return 8 * r / (4 * r ** 2 + (2 * pi * freqs) ** 2)


def fit_lorentz(freqs: NDArray[float64], Sxx: NDArray[float64]) -> NDArray[float64]:

    # Estimate for the shot noise
    B0 = mean(Sxx[freqs > 3e5])

    def loss(RB: NDArray[float64]) -> NDArray[float64]:
        r, b = RB
        
        return (eval_lorentz(freqs, r) + b - Sxx)
    
    sol: OptimizeResult = least_squares(fun=loss, x0=[2e5, B0])
    
    if sol.success:   
        return sol.x
    
    raise RuntimeError(f"{sol.message}")


if __name__ == "__main__":

    # Loading dataset from Moodle
    DSETPATH = SFOLDER / "dataNoise.csv"
    SIGNALS = loadtxt(DSETPATH, delimiter=",", dtype=float64)

    # Plot some of the signals
    plot_signals(SIGNALS, cols=[1, 2, 5])
    
    # Calculate the averaged psd by averaging ffts and welch's method
    FREQS, SXX = averaged_psd(SIGNALS)
    FREQS_WELCH, SXX_WELCH = psd_welch(SIGNALS)

    # Calculate signal variances
    VAR_X2 = signal_variances(SIGNALS)
    VAR_X2_PS = psd_variance(FREQS, SXX)
    VAR_X2_PS_WELCH = psd_variance(FREQS_WELCH, SXX_WELCH)

    # Curve fitting to estimate Lorentzian fit
    RB = fit_lorentz(freqs=FREQS, Sxx=SXX)
    RB_welch = fit_lorentz(freqs=FREQS_WELCH, Sxx=SXX_WELCH)

    # Comparing signal and psd variances
    print(f"Average signal variance:      {VAR_X2:.3f} [A ** 2]")
    print(f"Variance from averaged FFT:   {VAR_X2_PS:.3f} [A ** 2]")
    print(f"Variance from Welch's Method: {VAR_X2_PS_WELCH:.3f} [A ** 2]")

    # Plot the power spectral density with fit
    plot_psd_w_fit(
        freqs=FREQS, 
        Sxx=SXX,
        freqs_welch=FREQS_WELCH,
        Sxx_welch=SXX_WELCH, 
        RB=RB,
        RB_welch=RB_welch
    )
